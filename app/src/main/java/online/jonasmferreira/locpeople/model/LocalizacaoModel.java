package online.jonasmferreira.locpeople.model;

import android.location.Location;

import java.util.Date;
import java.util.List;

import online.jonasmferreira.locpeople.domain.Localizacao;
import online.jonasmferreira.locpeople.domain.Usuario;

public class LocalizacaoModel {
    public static Localizacao save(Location l){
        Usuario user = UsuarioModel.getCurrent();
        if(user!=null) {
            Localizacao localizacao = new Localizacao();
            localizacao.usuarioId = user.usuarioId;
            localizacao.latitude = l.getLatitude();
            localizacao.longitude = l.getLongitude();
            localizacao.precisao = l.getAccuracy();
            localizacao.altitude = l.getAltitude();
            localizacao.velocidade = l.getSpeed();
            localizacao.direcao = l.getBearing();
            localizacao.endereco = "";
            localizacao.dhFix = new Date(l.getTime());
            localizacao.dhRegistro = new Date();
            if(localizacao.save() > 0){
                return localizacao;
            }
        }
        return null;
    }

    public static boolean delete(Localizacao loc){
        return loc.delete();
    }
    public static List<Localizacao> getList(){
        List<Localizacao> loc = Localizacao.find(Localizacao.class,"",new String[]{},null,"DH_REGISTRO ASC",null);
        return loc;
    }
}
