package online.jonasmferreira.locpeople.model;

import online.jonasmferreira.locpeople.domain.Usuario;

public class UsuarioModel {
    public static Usuario save(String usuarioId,String nome, String email){
        Usuario user = new Usuario();
        user.setId((long)1);
        user.usuarioId = usuarioId;
        user.usuarioNome = nome;
        user.usuarioEmail = email;

        if(user.save() > 0){
            return user;
        }
        return null;
    }

    public static Usuario getCurrent(){
        return Usuario.findById(Usuario.class,1);
    }

    public static boolean delUser(){
        return Usuario.findById(Usuario.class,1).delete();
    }
}
