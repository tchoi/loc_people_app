package online.jonasmferreira.locpeople.domain;

import com.orm.SugarRecord;

import online.jonasmferreira.locpeople.util.Helper;

public class Usuario extends SugarRecord{
    public String usuarioId;
    public String usuarioNome;
    public String usuarioEmail;
    public String dispositivoId;
    public String dispositivoModelo;
    public String dispositivoVersao;

    public Usuario() {
        this.dispositivoId = Helper.getDeviceId();
        this.dispositivoModelo = Helper.getDeviceName();
        this.dispositivoVersao = Helper.getAndroidVersion();
    }

    public Usuario(String usuarioId, String usuarioNome, String usuarioEmail, String dispositivoId, String dispositivoModelo, String dispositivoVersao) {
        this.usuarioId = usuarioId;
        this.usuarioNome = usuarioNome;
        this.usuarioEmail = usuarioEmail;
        this.dispositivoId = dispositivoId;
        this.dispositivoModelo = dispositivoModelo;
        this.dispositivoVersao = dispositivoVersao;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "usuarioId='" + usuarioId + '\'' +
                ", usuarioNome='" + usuarioNome + '\'' +
                ", usuarioEmail='" + usuarioEmail + '\'' +
                ", dispositivoId='" + dispositivoId + '\'' +
                ", dispositivoModelo='" + dispositivoModelo + '\'' +
                ", dispositivoVersao='" + dispositivoVersao + '\'' +
                '}';
    }
}
