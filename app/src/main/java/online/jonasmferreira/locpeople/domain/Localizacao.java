package online.jonasmferreira.locpeople.domain;

import com.orm.SugarRecord;

import java.util.Date;

public class Localizacao extends SugarRecord {
    public String usuarioId;
    public Date dhRegistro;
    public Date dhFix;
    public double latitude;
    public double longitude;
    public float direcao;
    public float precisao;
    public float velocidade;
    public double altitude;
    public String endereco;

    public Localizacao() {
    }

    public Localizacao(String usuarioId, Date dhRegistro, Date dhFix, double latitude, double longitude, float direcao, float precisao, float velocidade, double altitude, String endereco) {
        this.usuarioId = usuarioId;
        this.dhRegistro = dhRegistro;
        this.dhFix = dhFix;
        this.latitude = latitude;
        this.longitude = longitude;
        this.direcao = direcao;
        this.precisao = precisao;
        this.velocidade = velocidade;
        this.altitude = altitude;
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        return "Localizacao{" +
                "usuarioId='" + usuarioId + '\'' +
                ", dhRegistro=" + dhRegistro +
                ", dhFix='" + dhFix + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", direcao=" + direcao +
                ", precisao=" + precisao +
                ", velocidade=" + velocidade +
                ", altitude=" + altitude +
                ", endereco='" + endereco + '\'' +
                '}';
    }
}
