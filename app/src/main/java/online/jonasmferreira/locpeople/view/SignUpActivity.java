package online.jonasmferreira.locpeople.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import online.jonasmferreira.locpeople.R;
import online.jonasmferreira.locpeople.controller.UsuarioController;
import online.jonasmferreira.locpeople.util.Helper;
import online.jonasmferreira.locpeople.util.MyProgressDialog;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{
    protected static String TAG = SignUpActivity.class.getSimpleName();
    private EditText etName;
    private EditText etEmail;
    private EditText etSenha;
    private AppCompatButton btnSignup;
    private TextView tvLogin;
    private Context context;
    private BroadcastReceiver broadReceiver;
    private MyProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_sign_up);
        initView();
    }

    private void initView() {
        progressDialog = new MyProgressDialog(this);
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etSenha = (EditText) findViewById(R.id.etSenha);
        btnSignup = (AppCompatButton) findViewById(R.id.btnSignup);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        btnSignup.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnSignup:{
                validate();
                break;
            }
            case R.id.tvLogin:{
                Helper.goTo(context,LoginActivity.class);
                break;
            }
        }

    }

    protected void validate(){
        String nome = etName.getText().toString();
        String email = etEmail.getText().toString();
        String senha = etSenha.getText().toString();
        boolean valid = true;
        if(nome.isEmpty() || nome.length() < 5){
            etName.setError(String.format(getResources().getString(R.string.erroMinChar), "Nome","5"));
            valid = false;
        }else{
            etName.setError(null);
        }

        if(email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etEmail.setError(getResources().getString(R.string.erroEmailInvalido));
            valid = false;
        }else{
            etEmail.setError(null);
        }

        if(senha.isEmpty() || senha.length() < 5){
            etSenha.setError(String.format(getResources().getString(R.string.erroMinChar), "Senha","5"));
            valid = false;
        }else{
            etSenha.setError(null);
        }

        if(valid){
            progressDialog.show();
            UsuarioController.signUp(nome,email,senha);
        }
    }

    private void loadBroadcasts(){
        broadReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive (Context context, Intent intent){
                String action = intent.getAction();
                Log.d(TAG, "onReceive: Action->"+action);
                closeProgressDialog();
                switch (action)
                {
                    case "locPeopleSignUp":
                    {
                        if (intent.getExtras().getBoolean("Success", false)){
                            alert("Sucesso",intent.getStringExtra("Message"),MainActivity.class);
                        }else{
                            Log.e(TAG, "onReceive: Falha na sincronicação: "+intent.getStringExtra("Message"));
                            alert("Erro",intent.getStringExtra("Message"));
                        }
                        break;
                    }
                    default:{

                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("locPeopleSignUp");
        LocalBroadcastManager.getInstance(this).registerReceiver(broadReceiver, filter);
    }

    protected void alert(String title, String msg) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }

    protected void alert(String title, String msg, final Class<?> classe) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Helper.goTo(context,classe);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }

    protected void closeProgressDialog(){
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    protected void onResume(){
        super.onResume();
        loadBroadcasts();
    }

    @Override
    protected void onPause (){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadReceiver);
        closeProgressDialog();
    }
}
