package online.jonasmferreira.locpeople.view;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import online.jonasmferreira.locpeople.R;
import online.jonasmferreira.locpeople.broadcast.StartServicesReceiver;
import online.jonasmferreira.locpeople.controller.UsuarioController;
import online.jonasmferreira.locpeople.domain.Usuario;
import online.jonasmferreira.locpeople.model.UsuarioModel;
import online.jonasmferreira.locpeople.service.LocalizacaoService;
import online.jonasmferreira.locpeople.util.Helper;
import online.jonasmferreira.locpeople.util.MyProgressDialog;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    protected static String TAG = LoginActivity.class.getSimpleName();

    private EditText etEmail;
    private EditText etSenha;
    private AppCompatButton btnlogin;
    private TextView tvSignup;
    private BroadcastReceiver broadReceiver;
    private MyProgressDialog progressDialog;
    private Context context;

    protected static final int RQ_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        initView();
        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,RQ_LOCATION);
        askForPermission(Manifest.permission.ACCESS_COARSE_LOCATION, RQ_LOCATION);
    }

    private void initView() {
        progressDialog = new MyProgressDialog(this);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etSenha = (EditText) findViewById(R.id.etSenha);
        btnlogin = (AppCompatButton) findViewById(R.id.btnlogin);
        tvSignup = (TextView) findViewById(R.id.tvSignup);

        btnlogin.setOnClickListener(this);
        tvSignup.setOnClickListener(this);
    }

    protected void alert(String title, String msg) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }

    protected void alert(String title, String msg, final Class<?> classe) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Helper.goTo(context,classe);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }

    private void loadBroadcasts(){
        broadReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive (Context context, Intent intent){
                String action = intent.getAction();
                Log.d(TAG, "onReceive: Action->"+action);
                closeProgressDialog();
                switch (action)
                {
                    case "locPeopleLogin":
                    {
                        if (intent.getExtras().getBoolean("Success", false)){
                            alert("Sucesso",intent.getStringExtra("Message"),MainActivity.class);
                        }else{
                            Log.e(TAG, "onReceive: Falha na sincronicação");
                            alert("Erro",intent.getStringExtra("Message"));
                        }
                        break;
                    }
                    default:{

                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("locPeopleLogin");
        LocalBroadcastManager.getInstance(this).registerReceiver(broadReceiver, filter);
    }

    protected void closeProgressDialog(){
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    protected void onResume(){
        super.onResume();
        loadBroadcasts();
        if(UsuarioModel.getCurrent()!=null){
            Helper.goTo(context,MainActivity.class);
            finish();
        }
        if(!Helper.isServiceUp(LocalizacaoService.class,this)){
            sendBroadcast(new Intent(context, StartServicesReceiver.class));
        }
    }

    @Override
    protected void onPause (){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadReceiver);
        closeProgressDialog();
    }

    protected void validate(){
        String email = etEmail.getText().toString();
        String senha = etSenha.getText().toString();
        boolean valid = true;

        if(email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etEmail.setError(getResources().getString(R.string.erroEmailInvalido));
            valid = false;
        }else{
            etEmail.setError(null);
        }

        if(senha.isEmpty() || senha.length() < 5){
            etSenha.setError(String.format(getResources().getString(R.string.erroMinChar), "Senha","5"));
            valid = false;
        }

        if(valid){
            progressDialog.show();
            UsuarioController.login(email,senha);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnlogin:{
                validate();
                break;
            }
            case R.id.tvSignup:{
                Helper.goTo(context,SignUpActivity.class);
                break;
            }
        }
    }
    private void askForPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            execPermissionGranted(requestCode);
        }
    }

    protected void execPermissionGranted(int requestCode){
        switch(requestCode){
            case RQ_LOCATION:{
                break;
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED){
            execPermissionGranted(requestCode);
        }
    }
}
