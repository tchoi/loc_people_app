package online.jonasmferreira.locpeople.view;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import online.jonasmferreira.locpeople.R;
import online.jonasmferreira.locpeople.broadcast.StartServicesReceiver;
import online.jonasmferreira.locpeople.controller.LocalizacaoController;
import online.jonasmferreira.locpeople.controller.UsuarioController;
import online.jonasmferreira.locpeople.model.UsuarioModel;
import online.jonasmferreira.locpeople.service.LocalizacaoService;
import online.jonasmferreira.locpeople.util.Helper;
import online.jonasmferreira.locpeople.util.MyProgressDialog;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
    protected static final int RQ_LOCATION = 1, RQ_LOCATION_2 = 2;
    private static String TAG = MainActivity.class.getSimpleName();
    private Button btnLogoff;
    private BroadcastReceiver broadReceiver;
    private MyProgressDialog progressDialog;
    private Context context;
    private GoogleMap mMap;
    private Button btnMaps;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter, dateFormatter2;
    private TextView tvDataInicial;
    private ImageView btDataInicial;
    private TextView tvDataFinal;
    private ImageView btDataFinal;
    private ImageView btBuscar;
    private long dataInicio = 0L, dataFim = 0L;
    private FusedLocationProviderClient mFusedLocationClient;
    List<Marker> markers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);
        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION, RQ_LOCATION);
        askForPermission(Manifest.permission.ACCESS_COARSE_LOCATION, RQ_LOCATION_2);
        initView();
    }


    private void askForPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            //Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
            execPermissionGranted(requestCode);
        }
    }

    protected void execPermissionGranted(int requestCode) {
        switch (requestCode) {
            case RQ_LOCATION: {
                break;
            }
            case RQ_LOCATION_2: {
                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            execPermissionGranted(requestCode);
        }
    }

    private void initView() {
        progressDialog = new MyProgressDialog(context);
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        dateFormatter2 = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tvDataInicial = (TextView) findViewById(R.id.tvDataInicial);
        tvDataInicial.setOnClickListener(this);

        btDataInicial = (ImageView) findViewById(R.id.btDataInicial);
        btDataInicial.setOnClickListener(this);

        tvDataFinal = (TextView) findViewById(R.id.tvDataFinal);
        tvDataFinal.setOnClickListener(this);

        btDataFinal = (ImageView) findViewById(R.id.btDataFinal);
        btDataFinal.setOnClickListener(this);

        btBuscar = (ImageView) findViewById(R.id.btBuscar);
        btBuscar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDataInicial: {
                showDateTimePicker(tvDataInicial);
                break;
            }
            case R.id.btDataInicial: {
                showDateTimePicker(tvDataInicial);
                break;
            }
            case R.id.tvDataFinal: {
                showDateTimePicker(tvDataFinal);
                break;
            }
            case R.id.btDataFinal: {
                showDateTimePicker(tvDataFinal);
                break;
            }
            case R.id.btBuscar: {

                if(dataInicio > 0 && dataFim > 0 && dataInicio <= dataFim) {
                    progressDialog.show();
                    LocalizacaoController.filter(Helper.dateDB(new Date(dataInicio)), Helper.dateDB(new Date(dataFim)));
                }else{
                    alert("Atenção","Datas do filtro inválidas");
                }
            }
        }
    }

    private void loadBroadcasts() {
        broadReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.d(TAG, "onReceive: Action->" + action);
                closeProgressDialog();
                switch (action) {
                    case "locPeopleLogoff": {
                        if (intent.getExtras().getBoolean("Success", false)) {
                            alert("Sucesso", intent.getStringExtra("Message"), LoginActivity.class);
                            UsuarioModel.delUser();
                        } else {
                            Log.e(TAG, "onReceive: Falha na sincronicação");
                            alert("Erro", intent.getStringExtra("Message"));
                        }
                        break;
                    }
                    case "locPeopleLocalizacaoFilter": {
                        if (intent.getExtras().getBoolean("Success", false)) {
                            markers.clear();
                            posAtual();
                            try{
                                JSONArray arr = new JSONArray(intent.getExtras().getString("Data",""));
                                for(int i=0;i<arr.length();i++){
                                    JSONObject o = arr.getJSONObject(i);
                                    LatLng postAtual = new LatLng(
                                            Double.parseDouble(o.getString("latitude")),
                                            Double.parseDouble(o.getString("longitude"))
                                    );
                                    String dt = dateFormatter.format(Helper.dateBR2DB(o.getString("dh_registro")));
                                    markers.add(mMap.addMarker(new MarkerOptions().position(postAtual).title("Data: "+dt)));
                                    mMap.moveCamera(CameraUpdateFactory.newLatLng(postAtual));
                                }
                                ajusteZoom();
                            }catch (Exception e){

                            }
                        } else {
                            Log.e(TAG, "onReceive: Falha na sincronicação");
                            alert("Erro", intent.getStringExtra("Message"));
                        }
                        break;
                    }
                    default: {

                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("locPeopleLogoff");
        filter.addAction("locPeopleLocalizacaoFilter");
        LocalBroadcastManager.getInstance(this).registerReceiver(broadReceiver, filter);
    }

    protected void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadBroadcasts();
        if (!Helper.isServiceUp(LocalizacaoService.class, this)) {
            sendBroadcast(new Intent(context, StartServicesReceiver.class));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadReceiver);
        closeProgressDialog();
    }

    protected void alert(String title, String msg) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }

    protected void alert(String title, String msg, final Class<?> classe) {

        TextView tv1 = new TextView(context);
        tv1.setPadding(30, 30, 30, 30);
        tv1.setText(title);
        tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv1.setTextColor(Color.BLACK);


        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCustomTitle(tv1);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Helper.goTo(context, classe);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView tv2 = (TextView) alertDialog.findViewById(android.R.id.message);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMaxZoomPreference(21);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        posAtual();
        ajusteZoom();

    }

    protected void posAtual(){
        LatLng postAtual = new LatLng(
                Double.parseDouble(Helper.getPreferences("lat")),
                Double.parseDouble(Helper.getPreferences("lng"))
        );
        markers.add(mMap.addMarker(new MarkerOptions().position(postAtual).title("Posição atual")));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(postAtual));
    }

    protected void ajusteZoom(){
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker m : markers) {
            builder.include(m.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.12);
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater findMenuItems = getMenuInflater();
        findMenuItems.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logoff:
                UsuarioController.logoff();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    protected void showDateTimePicker(final TextView tv) {
        final Calendar currentDate = Calendar.getInstance();
        final Calendar date = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        date.set(Calendar.SECOND, 0);
                        // Log.v(TAG, "The choosen one " + date.getTime());
                        // Toast.makeText(getContext(),"The choosen one " + date.getTime(),Toast.LENGTH_SHORT).show();
                        tv.setText(dateFormatter.format(date.getTime()));
                        if(tv.getId()==R.id.tvDataInicial || tv.getId()==R.id.btDataInicial){
                            dataInicio = date.getTime().getTime();
                        }else if(tv.getId()==R.id.tvDataFinal || tv.getId()==R.id.btDataFinal){
                            dataFim = date.getTime().getTime();
                        }
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), true).show();

            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE));
        fromDatePickerDialog.getDatePicker().setMaxDate(currentDate.getTimeInMillis());
        if (Build.VERSION.SDK_INT <= 21) {
            fromDatePickerDialog.getDatePicker().setCalendarViewShown(true);
            fromDatePickerDialog.getDatePicker().setSpinnersShown(false);
        }
        fromDatePickerDialog.show();
    }
}
