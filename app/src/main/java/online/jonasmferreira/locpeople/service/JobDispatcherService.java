package online.jonasmferreira.locpeople.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import online.jonasmferreira.locpeople.broadcast.LocalizacaoReceiver;
import online.jonasmferreira.locpeople.controller.LocalizacaoController;
import online.jonasmferreira.locpeople.domain.Localizacao;
import online.jonasmferreira.locpeople.util.Helper;

public class JobDispatcherService extends JobService {
    protected static String TAG = JobDispatcherService.class.getSimpleName();
    @Override
    public boolean onStartJob(JobParameters job) {
        // Do some work here
        Log.d(TAG, "onStartJob: Start");
        switch(job.getTag()){
            case "monitoring-service":{
                if(!Helper.isServiceUp(LocalizacaoService.class,this)){
                    sendBroadcast(new Intent(this, LocalizacaoReceiver.class));
                }
                LocalizacaoController.sendLocalizacoes();
                break;
            }

        }

        return false; // Answers the question: "Is there still work going on?"
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        switch(job.getTag()){
            case "monitoring-service":{
                if(Helper.isServiceUp(LocalizacaoService.class,this)){
                   stopService(new Intent(this,LocalizacaoService.class));
                }
                break;
            }
        }
        return false; // Answers the question: "Should this job be retried?"
    }
}