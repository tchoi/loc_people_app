package online.jonasmferreira.locpeople.util;

import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import java.lang.reflect.Method;

public class App extends com.orm.SugarApp {
    public static final String TAG = App.class.getSimpleName();
    private static App mInstance;
    public static synchronized App getInstance() {
        return mInstance;
    }
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
