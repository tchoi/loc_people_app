package online.jonasmferreira.locpeople.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper {
    public static String TAG = Helper.class.getSimpleName();
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    public static String getAndroidVersion(){
        return Build.VERSION.RELEASE;
    }

    public static String getDeviceId(){
        return Settings.Secure.getString(App.getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String md5(String senha){
        String sen = "";
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
        sen = hash.toString(16);
        return sen;
    }

    public static void sendLocalBroadcast(Context lContext, String action, Bundle bundle) {
        Intent intent = new Intent(action);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        LocalBroadcastManager.getInstance(lContext).sendBroadcast(intent);
    }

    public static void goTo(Context lcontext, Class<?> lcls) {
        Intent intent = new Intent(lcontext, lcls);
        intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        lcontext.startActivity(intent);
    }

    public static boolean isServiceUp(Class<?> serviceClass, Context context){
        boolean retorno = false;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if ( serviceClass.getName().equals( service.service.getClassName() ) ){
                retorno = true;
                break;
            }
        }
        return retorno;
    }

    public static boolean isServiceUp(String serviceClass, Context context){
        boolean retorno = false;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if ( serviceClass.equals( service.service.getClassName() ) ){
                retorno = true;
                break;
            }
        }
        return retorno;
    }

    public static String dateDB(Date today){
        return dateFormat(today,"yyyy-MM-dd HH:mm:ss");
    }

    public static String dateFormat(Date today, String format){
        SimpleDateFormat df = new SimpleDateFormat(format);
        String dt = df.format(today);
        return dt;
    }

    public static void setPreference(String key,String value){
        SharedPreferences sharedpreferences = App.getInstance().getSharedPreferences("LocPeoplePref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static String getPreferences(String key){
        SharedPreferences sharedpreferences = App.getInstance().getSharedPreferences("LocPeoplePref", Context.MODE_PRIVATE);
        return sharedpreferences.getString(key,"");
    }
    public static Date dateBR2DB(String dt,String format){
        Date date;

        if(dt==null){
            return new Date();
        }

        SimpleDateFormat df = new SimpleDateFormat(format);
        try {
            date = df.parse(dt);
            return date;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static Date dateBR2DB(String dt){
        return dateBR2DB(dt,"yyyy-MM-dd HH:mm:ss");
    }
}
