package online.jonasmferreira.locpeople.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import online.jonasmferreira.locpeople.service.LocalizacaoService;

public class LocalizacaoReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Intent it = new Intent(context, LocalizacaoService.class);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            context.startService(it);
        }else {
            context.startForegroundService(it);
        }
    }
}
