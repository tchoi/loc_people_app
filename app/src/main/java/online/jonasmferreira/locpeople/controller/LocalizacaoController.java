package online.jonasmferreira.locpeople.controller;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import online.jonasmferreira.locpeople.R;
import online.jonasmferreira.locpeople.domain.Localizacao;
import online.jonasmferreira.locpeople.model.LocalizacaoModel;
import online.jonasmferreira.locpeople.model.UsuarioModel;
import online.jonasmferreira.locpeople.util.App;
import online.jonasmferreira.locpeople.util.Helper;

public class LocalizacaoController {
    protected static String url = App.getInstance().getString(R.string.SERVER_BASE_URL);
    protected static String TAG = LocalizacaoController.class.getSimpleName();
    public static void sendLocalizacao(final Localizacao localizacao){
        if(UsuarioModel.getCurrent()==null){
            return;
        }
        final String ENDPOINT = url+"localizacao";
        final Context context = App.getInstance();
        JSONArray localizacoes = new JSONArray();
        try{
            JSONObject loc = new JSONObject();
            loc.put("dh_registro",Helper.dateDB(localizacao.dhRegistro));
            loc.put("dh_fix",Helper.dateDB(localizacao.dhFix));
            loc.put("latitude",localizacao.latitude);
            loc.put("longitude",localizacao.longitude);
            loc.put("direcao",localizacao.direcao);
            loc.put("precisao",localizacao.precisao);
            loc.put("velocidade",localizacao.velocidade);
            loc.put("altitude",localizacao.altitude);
            loc.put("endereco",localizacao.endereco);
            localizacoes.put(loc);
        }catch (Exception e){
            e.printStackTrace();
        }

        final RequestParams params = new RequestParams();
        params.put("token", Helper.md5(UsuarioModel.getCurrent().usuarioId));
        params.put("localizacoes", localizacoes.toString());

        List<Localizacao> list = new ArrayList<>();
        list.add(localizacao);
        sendLocs(params,list);
    }

    public static void sendLocalizacoes(){
        if(UsuarioModel.getCurrent()==null){
            return;
        }
        JSONArray localizacoes = new JSONArray();
        List<Localizacao> list =LocalizacaoModel.getList();
        if(list.size() > 0){
            for (Localizacao localizacao: list) {
                try{
                    JSONObject loc = new JSONObject();
                    loc.put("dh_registro",Helper.dateDB(localizacao.dhRegistro));
                    loc.put("dh_fix",Helper.dateDB(localizacao.dhFix));
                    loc.put("latitude",localizacao.latitude);
                    loc.put("longitude",localizacao.longitude);
                    loc.put("direcao",localizacao.direcao);
                    loc.put("precisao",localizacao.precisao);
                    loc.put("velocidade",localizacao.velocidade);
                    loc.put("altitude",localizacao.altitude);
                    loc.put("endereco",localizacao.endereco);
                    localizacoes.put(loc);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            final RequestParams params = new RequestParams();
            params.put("token", Helper.md5(UsuarioModel.getCurrent().usuarioId));
            params.put("localizacoes", localizacoes.toString());


            sendLocs(params,list);
        }

    }

    protected static void sendLocs(final RequestParams params, final List<Localizacao> list){
        final String ENDPOINT = url+"localizacao/";
        final Context context = App.getInstance();

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60*1000);
        client.post(ENDPOINT, params, new JsonHttpResponseHandler()
        {

            @Override
            public void onStart (){
                Log.d(TAG, "onStart: params->"+params.toString());
            }

            @Override
            public void onSuccess (int statusCode, Header[] headers, JSONObject response){
                Log.d(TAG, "onSuccess: response->"+response.toString());
                try{
                    JSONObject data = response.getJSONObject("data");
                    if(data.getBoolean("success")){
                        for(Localizacao localizacao: list){
                            localizacao.delete();
                        }

                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){
                String replyMsg = "Falha " + statusCode + ": " + throwable.getLocalizedMessage();
                Log.e(TAG, "onFailure: "+replyMsg+"->"+(errorResponse != null ? errorResponse.toString() : "nl | " +  throwable.getMessage()));

                if(statusCode==409) {

                }else {
                    String msg = "Falha de conexão com o Servidor [" + statusCode + "]";
                    try {
                        msg = errorResponse.has("msg") ? errorResponse.getString("msg") : replyMsg;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse){
                String replyMsg = "Falha " + statusCode + ": " + throwable.getLocalizedMessage();
                Log.e(TAG, "onFailure: "+replyMsg+"->"+(errorResponse != null ? errorResponse.toString() : "nl | " +  throwable.getMessage()));
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, String responseString, Throwable throwable){
                String replyMsg = "Falha " + statusCode + ": " + throwable.getLocalizedMessage();
                Log.e(TAG, "onFailure: "+replyMsg+"->"+(responseString != null ? responseString.toString() : "nl | " +  throwable.getMessage()));
            }
            @Override
            public void onFinish(){

            }
        });
    }

    public static void filter(final String data_inicio, final String data_fim){
        final String broadcastAction = "locPeopleLocalizacaoFilter";
        final String ENDPOINT = url+"localizacao/filter";
        final Context context = App.getInstance();
        final RequestParams params = new RequestParams();
        params.put("token", Helper.md5(UsuarioModel.getCurrent().usuarioId));
        params.put("data_inicio", data_inicio);
        params.put("data_fim", data_fim);

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60*1000);
        client.post(ENDPOINT, params, new JsonHttpResponseHandler()
        {
            @Override
            public void onStart (){
                Log.d(TAG, "onStart: params->"+params.toString());
            }

            @Override
            public void onSuccess (int statusCode, Header[] headers, JSONObject response){
                Bundle broadBundle = new Bundle();
                try{
                    Log.d(TAG, "onSuccess: response->"+response.toString());
                    JSONObject data = response.getJSONObject("data");
                    JSONArray locs = data.getJSONArray("locs");
                    broadBundle.putBoolean("Success", true);
                    broadBundle.putString("Message", data.getString("msg"));
                    broadBundle.putString("Data", locs.toString());

                }catch (Exception ex){
                    ex.printStackTrace();
                    broadBundle.putBoolean("Success", false);
                    Log.e(TAG, "onSuccess: Exception->"+ex.getLocalizedMessage());
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){
                String replyMsg = "Falha " + statusCode + ": " + throwable.getLocalizedMessage();
                Log.e(TAG, "onFailure: "+replyMsg+"->"+(errorResponse != null ? errorResponse.toString() : "nl | " +  throwable.getMessage()));
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);
                }else {
                    String msg = "Falha de conexão com o Servidor [" + statusCode + "]";
                    try {
                        msg = errorResponse.has("msg") ? errorResponse.getString("msg") : replyMsg;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    broadBundle.putString("Message", msg);
                    broadBundle.putBoolean("Success", false);

                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse){
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);
                }else {
                    broadBundle.putBoolean("Success", false);
                    broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, String responseString, Throwable throwable){
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);

                }else{
                    broadBundle.putBoolean("Success", false);
                    broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }
            @Override
            public void onFinish(){

            }
        });

    }


}
