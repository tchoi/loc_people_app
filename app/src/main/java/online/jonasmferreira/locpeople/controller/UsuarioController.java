package online.jonasmferreira.locpeople.controller;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import online.jonasmferreira.locpeople.R;
import online.jonasmferreira.locpeople.domain.Usuario;
import online.jonasmferreira.locpeople.model.UsuarioModel;
import online.jonasmferreira.locpeople.util.App;
import online.jonasmferreira.locpeople.util.Helper;

public class UsuarioController {
    protected static String url = App.getInstance().getString(R.string.SERVER_BASE_URL);
    protected static String TAG = UsuarioController.class.getSimpleName();
    public static void login(String email, String senha){
        final String broadcastAction = "locPeopleLogin";
        final String ENDPOINT = url+"login";
        final Context context = App.getInstance();
        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("senha", Helper.md5(senha));
        params.put("dispositivo_id", Helper.getDeviceId());
        params.put("dispositivo_modelo", Helper.getDeviceName());
        params.put("dispositivo_versao", Helper.getAndroidVersion());

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60*1000);
        client.post(ENDPOINT, params, new JsonHttpResponseHandler()
        {

            @Override
            public void onStart (){
            }

            @Override
            public void onSuccess (int statusCode, Header[] headers, JSONObject response){
                Log.d(TAG, "onSuccess: response->"+response.toString());
                Bundle broadBundle = new Bundle();
                try{
                    JSONObject data = response.getJSONObject("data");
                    JSONObject user = data.getJSONObject("user");
                    broadBundle.putBoolean("Success", true);
                    broadBundle.putString("Message", data.getString("msg"));
                    UsuarioModel.save(user.getString("id"),user.getString("nome"),user.getString("email"));

                }catch (Exception ex){
                    ex.printStackTrace();
                    broadBundle.putBoolean("Success", false);
                    Log.e(TAG, "onSuccess: Exception->"+ex.getLocalizedMessage());
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){
                String replyMsg = "Falha " + statusCode + ": " + throwable.getLocalizedMessage();
                Log.e(TAG, "onFailure: "+replyMsg+"->"+(errorResponse != null ? errorResponse.toString() : "nl | " +  throwable.getMessage()));
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);
                }else {
                    String msg = "Falha de conexão com o Servidor [" + statusCode + "]";
                    try {
                        msg = errorResponse.has("msg") ? errorResponse.getString("msg") : replyMsg;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    broadBundle.putString("Message", msg);
                    broadBundle.putBoolean("Success", false);

                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse){
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);
                }else {
                    broadBundle.putBoolean("Success", false);
                    broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, String responseString, Throwable throwable){
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);

                }else{
                    broadBundle.putBoolean("Success", false);
                    broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }
            @Override
            public void onFinish(){

            }
        });

    }
    public static void signUp(final String nome, final String email, String senha){
        final String broadcastAction = "locPeopleSignUp";
        final String ENDPOINT = url+"usuario/";
        final Context context = App.getInstance();
        RequestParams params = new RequestParams();
        params.put("nome",nome);
        params.put("email",email);
        params.put("senha", Helper.md5(senha));
        params.put("dispositivo_id", Helper.getDeviceId());
        params.put("dispositivo_modelo", Helper.getDeviceName());
        params.put("dispositivo_versao", Helper.getAndroidVersion());

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60*1000);
        client.post(ENDPOINT, params, new JsonHttpResponseHandler()
        {

            @Override
            public void onStart (){
            }

            @Override
            public void onSuccess (int statusCode, Header[] headers, JSONObject response){
                Log.d(TAG, "onSuccess: response->"+response.toString());
                Bundle broadBundle = new Bundle();
                try{
                    JSONObject data = response.getJSONObject("data");
                    JSONObject user = data.getJSONObject("user");
                    broadBundle.putBoolean("Success", true);
                    broadBundle.putString("Message", data.getString("msg"));
                    UsuarioModel.save(user.getString("id"),nome,email);
                }catch (Exception ex){
                    ex.printStackTrace();
                    broadBundle.putBoolean("Success", false);
                    Log.e(TAG, "onSuccess: Exception->"+ex.getLocalizedMessage());
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){
                String replyMsg = "Falha " + statusCode + ": " + throwable.getLocalizedMessage();
                Log.e(TAG, "onFailure: "+replyMsg+"->"+(errorResponse != null ? errorResponse.toString() : "nl | " +  throwable.getMessage()));
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);
                }else {
                    String msg = "Falha de conexão com o Servidor [" + statusCode + "]";
                    try {
                        msg = errorResponse.has("msg") ? errorResponse.getString("msg") : replyMsg;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    broadBundle.putString("Message", msg);
                    broadBundle.putBoolean("Success", false);

                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse){
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);
                }else {
                    broadBundle.putBoolean("Success", false);
                    broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, String responseString, Throwable throwable){
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);

                }else{
                    broadBundle.putBoolean("Success", false);
                    broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }
            @Override
            public void onFinish(){

            }
        });

    }
    public static void logoff(){
        final String broadcastAction = "locPeopleLogoff";
        final String ENDPOINT = url+"logoff";
        final Context context = App.getInstance();
        final RequestParams params = new RequestParams();
        params.put("id",Helper.md5(UsuarioModel.getCurrent().usuarioId));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(60*1000);
        client.post(ENDPOINT, params, new JsonHttpResponseHandler()
        {

            @Override
            public void onStart (){
                Log.d(TAG, "onStart: params"+params.toString());
            }

            @Override
            public void onSuccess (int statusCode, Header[] headers, JSONObject response){
                Log.d(TAG, "onSuccess: response->"+response.toString());
                Bundle broadBundle = new Bundle();
                try{
                    JSONObject data = response.getJSONObject("data");
                    broadBundle.putBoolean("Success", true);
                    broadBundle.putString("Message", data.getString("msg"));
                }catch (Exception ex){
                    ex.printStackTrace();
                    broadBundle.putBoolean("Success", false);
                    Log.e(TAG, "onSuccess: Exception->"+ex.getLocalizedMessage());
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){
                String replyMsg = "Falha " + statusCode + ": " + throwable.getLocalizedMessage();
                Log.e(TAG, "onFailure: "+replyMsg+"->"+(errorResponse != null ? errorResponse.toString() : "nl | " +  throwable.getMessage()));
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);
                }else {
                    String msg = "Falha de conexão com o Servidor [" + statusCode + "]";
                    try {
                        msg = errorResponse.has("msg") ? errorResponse.getString("msg") : replyMsg;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    broadBundle.putString("Message", msg);
                    broadBundle.putBoolean("Success", false);

                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse){
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);
                }else {
                    broadBundle.putBoolean("Success", false);
                    broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }

            @Override
            public void onFailure (int statusCode, Header[] headers, String responseString, Throwable throwable){
                Bundle broadBundle = new Bundle();
                if(statusCode==409) {
                    Toast.makeText(context, "Usuário logado em outro dispositivo", Toast.LENGTH_LONG).show();
                    //UserModel.SetUserLogged(false);
                    //Tools.goTo(context, LoginActivity.class);

                }else{
                    broadBundle.putBoolean("Success", false);
                    broadBundle.putString("Message", "Falha de conexão com o Servidor [" + statusCode + "]");
                }
                Helper.sendLocalBroadcast(context, broadcastAction, broadBundle);
            }
            @Override
            public void onFinish(){

            }
        });

    }
}
